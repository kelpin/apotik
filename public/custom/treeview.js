var Treeview = function () {

    var demo4 = function () {
        $('#m_tree_4').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes" : {
                    "responsive": true
                },    
                'data': [
                        {
                            "text": "Dashboard",
                            "icon": "flaticon-line-graph"
                        },
                        {
                            "text": "Master",
                            "icon" : "flaticon-layers",
                            "children": [
                                {
                                    "text": "Supllier",
                                    "icon": "fa fa-warning m--font-danger",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Obat",
                                    "icon" : "fa fa-folder m--font-default",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Pengguna",
                                    "icon": "fa fa-warning m--font-waring",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Roles",
                                    "icon": "fa fa-check m--font-success",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "text": "Transaksi",
                            "children": [
                                {
                                    "text": "Pembelian",
                                },{
                                    "text": "Penjualan",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                        {
                            "text": "Laporan",
                            "children": [
                                {
                                    "text": "Stok",
                                },
                                {
                                    "text": "Pembelian",
                                },{
                                    "text": "Penjualan",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                        {
                            "text": "Utility",
                            "children": [
                                {
                                    "text": "Konfigurasi Aplikasi",
                                },{
                                    "text": "Backup Database",
                                    "icon": "fa fa-warning m--font-danger"
                                },{
                                    "text": "Audit Trial",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                ]
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder m--font-warning"
                },
                "file" : {
                    "icon" : "fa fa-file  m--font-warning"
                }
            },
        });
    } 

    var demo5 = function () {
        $('#m_tree_3').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes" : {
                    "responsive": true
                },    
                'data': [
                        {
                            "text": "Dashboard",
                            "icon": "flaticon-line-graph"
                        },
                        {
                            "text": "Master",
                            "icon" : "flaticon-layers",
                            "children": [
                                {
                                    "text": "Supllier",
                                    "icon": "fa fa-warning m--font-danger",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Obat",
                                    "icon" : "fa fa-folder m--font-default",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Pengguna",
                                    "icon": "fa fa-warning m--font-waring",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }, {
                                    "text": "Roles",
                                    "icon": "fa fa-check m--font-success",
                                    "children": [
                                        {
                                            "text": "Aksi",
                                            "icon": "fa fa-warning m--font-danger",
                                            "children": [
                                                {
                                                    "text": "Create",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Update",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "Delete",
                                                    "icon": "fa fa-warning m--font-danger"
                                                },
                                                {
                                                    "text": "View",
                                                    "icon": "fa fa-warning m--font-danger"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "text": "Transaksi",
                            "children": [
                                {
                                    "text": "Pembelian",
                                },{
                                    "text": "Penjualan",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                        {
                            "text": "Laporan",
                            "children": [
                                {
                                    "text": "Stok",
                                },
                                {
                                    "text": "Pembelian",
                                },{
                                    "text": "Penjualan",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                        {
                            "text": "Utility",
                            "children": [
                                {
                                    "text": "Konfigurasi Aplikasi",
                                },{
                                    "text": "Backup Database",
                                    "icon": "fa fa-warning m--font-danger"
                                },{
                                    "text": "Audit Trial",
                                    "icon": "fa fa-warning m--font-danger"
                                }
                            ]
                        },
                ]
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder m--font-warning"
                },
                "file" : {
                    "icon" : "fa fa-file  m--font-warning"
                }
            },
        });
    } 

    var demo6 = function() {
        $("#m_tree_6").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return 'https://keenthemes.com/metronic/themes/themes/metronic/dist/preview/inc/api/jstree/ajax_data.php';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder m--font-brand"
                },
                "file" : {
                    "icon" : "fa fa-file  m--font-brand"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            demo4();
            demo5();
            demo6();
        }
    };
}();

jQuery(document).ready(function() {    
    Treeview.init();
});
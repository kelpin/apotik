<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load library phpspreadsheet
require('./Excel/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportController extends CI_Controller {

	public function ExportSupplier()
	{
        $allSupplier = $this->Mod_get->getAllSupplier()->result();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Kode Supplier')
                      ->setCellValue('C1', 'Nama Supplier')
                      ->setCellValue('D1', 'Alamat')
                      ->setCellValue('E1', 'Telepon');

          $kolom = 2;
          $nomor = 1;
          foreach($allSupplier as $supplier) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $nomor)
                           ->setCellValue('B' . $kolom, $supplier->kode_supplier)
                           ->setCellValue('C' . $kolom, $supplier->nama_supplier)
                           ->setCellValue('D' . $kolom, $supplier->alamat)
                           ->setCellValue('E' . $kolom, $supplier->telepon);

               $kolom++;
               $nomor++;

          }

          $writer = new Xlsx($spreadsheet);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Supplier('.date('Y-m-d').').xlsx"');
            header('Cache-Control: max-age=0');

	  $writer->save('php://output');
       
  }

  public function ExportObat()
	{
        $all = $this->Mod_get->getAllObat()->result();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Kode Obat')
                      ->setCellValue('C1', 'Nama Obat')
                      ->setCellValue('D1', 'Harga Beli')
                      ->setCellValue('E1', 'Harga Jual')
                      ->setCellValue('F1', 'Satuan')
                      ->setCellValue('G1', 'Min Stok')
                      ->setCellValue('H1', 'Stok');

          $kolom = 2;
          $nomor = 1;
          foreach($all as $obat) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $nomor)
                           ->setCellValue('B' . $kolom, $obat->kode_obat)
                           ->setCellValue('C' . $kolom, $obat->nama_obat)
                           ->setCellValue('D' . $kolom, $obat->harga_beli)
                           ->setCellValue('E' . $kolom, $obat->harga_jual)
                           ->setCellValue('F' . $kolom, $obat->nama_satuan)
                           ->setCellValue('G' . $kolom, $obat->min_stok)
                           ->setCellValue('H' . $kolom, $obat->stok);

               $kolom++;
               $nomor++;

          }

          $writer = new Xlsx($spreadsheet);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="ListObat('.date('Y-m-d').').xlsx"');
            header('Cache-Control: max-age=0');

	  $writer->save('php://output');
       
  }
  
  public function ExportUser()
	{
        $all = $this->Mod_get->getAllUser()->result();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'No')
                      ->setCellValue('B1', 'Nama User')
                      ->setCellValue('C1', 'Username')
                      ->setCellValue('D1', 'Hak Akses')
                      ->setCellValue('E1', 'Status');

          $kolom = 2;
          $nomor = 1;
          foreach($all as $obat) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $nomor)
                           ->setCellValue('B' . $kolom, $obat->nama_user)
                           ->setCellValue('C' . $kolom, $obat->username)
                           ->setCellValue('D' . $kolom, $obat->nama_akses)
                           ->setCellValue('E' . $kolom, $obat->blokir);

               $kolom++;
               $nomor++;

          }

          $writer = new Xlsx($spreadsheet);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="ListPengguna('.date('Y-m-d').').xlsx"');
            header('Cache-Control: max-age=0');

	  $writer->save('php://output');
       
	}

}

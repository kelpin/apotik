<?php
	$this->load->view('header');
?>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<?php $this->load->view('nav')?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<?php $this->load->view('sidebar')?>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav" id="filter">
									<li class="m-portlet__nav-item">
										<input type="text" class="form-control" id="dateAwal" readonly="" placeholder="Date Awal">
									</li>
									<li class="m-portlet__nav-item">
										<input type="text" class="form-control" id="dateAkhir" readonly="" placeholder="Date Akhir">
									</li>
										<li class="m-portlet__nav-item">
											<select name="" id="typeLap" onchange="lap()" class="form-control m-input">
												<option value=""> Type Laporan </option>
												<option value="seluruh"> Seluruh </option>
												<option value="minimm"> Minimum </option>
											</select>
										</li>
										<li class="m-portlet__nav-item">
											<a href="<?=base_url('exportSupplier')?>" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="fa fa-file-export"></i>
													<span>Export</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<table id="lapStok" class="table table-striped- table-bordered table-hover table-checkable">
									<thead>
										<tr>
											<th>Kode Obat</th>
											<th>Nama Obat</th>
											<th>Harga Beli</th>
											<th>Harga Jual</th>
											<th>Satuan</th>
											<th>Min Stok</th>
											<th>Stok</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>


<!-- <li class="m-portlet__nav-item">
	<a href="#" id="tampilkan"  class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
		<span>
			<span>Tampilkan</span>
		</span>
	</a>
</li> -->

<script type="text/javascript">
    var table;
    $(document).ready(function() {
		$('#dateAwal').hide()
		$('#dateAkhir').hide()

		
		

	});
	
	function lap() {
		var type = $("#typeLap").val()
		var uril = ""
		if (type =="seluruh"){
			uril = "getObat"
		}else{
			uril = "getMinimum"
		}
			table = $('#lapStok').DataTable({ 

				"processing": true, 
				"serverSide": true, 
				"order": [], 

				"ajax": {
					"url": '<?=base_url('')?>/'+uril,
					"type": "POST"
				},


				"columnDefs": [
				{ 
					"targets": [ 0 ], 
					"orderable": false, 
				},
				],

			});
			table.destroy()
	}


</script>
